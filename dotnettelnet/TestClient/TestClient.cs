/*
 * (c) Seva Petrov 2002-2003. All Rights Reserved.
 *
 * --LICENSE NOTICE--
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * --LICENSE NOTICE--
 *
 * $Date: 2003/05/31 02:30:57 $
 * $Id: TestClient.cs,v 1.3 2003/05/31 02:30:57 vnp Exp $
 * 
 */
using System;
using System.Threading;
using De.Mud.Telnet;

namespace Net.Graphite.Telnet
{
	/// <summary>
	/// A simple console testbed for the Telnet application. No terminal emulation
	/// is provided.
	/// </summary>
	public class TestClient
	{
		[STAThread]
		static void Main(string[] args)
		{
			if (args.Length < 3 || (args[0] != "i" && args[0] != "s"))
			{
				Console.WriteLine("Usage: TestClient [mode] [host] [port]");
				Console.WriteLine("mode: [i]nteractive/[s]cripted");
				return;
			}

			try 
			{
				Thread tSession;
				Session s = new Session(args[1], Int32.Parse(args[2]));
				Console.WriteLine("Connecting ...");
				if (s.Connect())
				{
					Console.WriteLine("Connected ...");


					if (args[0] == "i")
					{
						// this thread never dies with until the last blocking Console.ReadLine call
						// is completed
						tSession = new Thread(new ThreadStart(s.InteractiveSession));
						tSession.Start();
						while (!s.IsDone)
							Thread.Sleep(100);
					}
					else
					{
						tSession = new Thread(new ThreadStart(s.ScriptedSession));
						tSession.Start();
						while (!s.IsDone)
							Thread.Sleep(100);
					}
				}
				else
				{
					Console.WriteLine(Environment.NewLine + "Could not connect!");
				}
			}
			catch (ApplicationException e) 
			{
				Console.WriteLine("An exception has occurred: " + 
					e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine);
			} 
		}
	}

	/// <summary>
	/// A simple telnet session.
	/// </summary>
	public class Session
	{
		private TelnetWrapper t; 
		private bool done = false;

		/// <summary>
		/// Creates a new instance of the Session class with the specified host
		/// and port.
		/// </summary>
		/// <param name="host">Remote address/hostname</param>
		/// <param name="port">Remote port</param>
		public Session(string host, int port) 
		{
			t = new TelnetWrapper();
			
			t.Disconnected  += new DisconnectedEventHandler(this.OnDisconnect);
			t.DataAvailable += new DataAvailableEventHandler(this.OnDataAvailable);
			
			t.TerminalType = "NETWORK-VIRTUAL-TERMINAL";
			t.Hostname = host;
			t.Port = port;
		}

		/// <summary>
		/// Connects to the remote host.
		/// </summary>
		/// <returns>Success of connection.</returns>
		public bool Connect()
		{
			t.Connect();
			return t.Connected;
		}

		/// <summary>
		/// Gets the completion status of the current session.
		/// </summary>
		public bool IsDone
		{
			get
			{
				return done;
			}
		}

		/// <summary>
		/// Starts a new scripted session.
		/// </summary>
		public void ScriptedSession()
		{
			Console.WriteLine(Environment.NewLine + "Scripted sessions not implemented!");
			t.Disconnect();
			Console.ReadLine();
		}

		/// <summary>
		/// Starts a new interactive session.
		/// </summary>
		public void InteractiveSession()
		{
			try 
			{
				t.Receive();
				
				while (!done)
					t.Send(Console.ReadLine() + t.CR);
			}
			catch 
			{
				t.Disconnect();
				throw;
			}
		}

		private void OnDisconnect(object sender, EventArgs e)
		{
			done = true;
			Console.WriteLine(Environment.NewLine + "Disconnected.");
			Console.WriteLine("Press [Enter] to quit.");
		}

		private void OnDataAvailable(object sender, DataAvailableEventArgs e)
		{
			Console.Write(e.Data);
		}
	}
}
