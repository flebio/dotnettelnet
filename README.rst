===========
.NET Telnet
===========

This is a fork of *.NET Telnet* from
http://dotnettelnet.sourceforge.net/

About
-----

.NET Telnet is a telnet library written for the MS .NET Framework. It is inspired by the Java Telnet Application. 

License
-------

.NET Telnet is Free Software, released under the GPL.

Description
-----------

The current version is 1.0.0. It includes the library assembly and a simple test utility. The Telnet protocol is implemented; however, there is currently no support for terminal emulation. Development is organized around the project's SourceForge homepage.

Documentation
-------------

Browse the class library documentation. Courtesy of the NDoc project.

Setup
-----

The build and intermediate folders are created one directory above the root dir.
The following directory structure is recommended:

* dotnetTelnet

  * bin (Builds. Will be created.)

  * obj (Intermediate files. Will be created.)

  * src (root of source code. Put your git files here.)

    * dotnettelnet\

      * ... (source project)

    * README.rst (this file)
